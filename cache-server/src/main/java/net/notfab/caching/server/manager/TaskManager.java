package net.notfab.caching.server.manager;

import java.util.concurrent.*;

public class TaskManager {

    private ThreadPoolExecutor cachedExecutor;
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;

    public TaskManager() {
        this.cachedExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        this.scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(2);
        this.scheduledThreadPoolExecutor.setMaximumPoolSize(Integer.MAX_VALUE);
    }

    public void shutdown() {
        this.scheduledThreadPoolExecutor.shutdownNow();
        this.cachedExecutor.shutdownNow();
    }

    public ScheduledFuture<?> scheduleAtFixedRate(Runnable runnable, long initDelay, long rate, TimeUnit unit) {
        return scheduledThreadPoolExecutor.scheduleWithFixedDelay(runnable, initDelay, rate, unit);
    }

    public ScheduledFuture<?> runTaskLater(Runnable runnable, long delay, TimeUnit unit) {
        return scheduledThreadPoolExecutor.schedule(runnable, delay, unit);
    }

    public Future<?> runAsync(Runnable runnable) {
        return this.cachedExecutor.submit(runnable);
    }

}
