package net.notfab.caching.server.entities;

import com.sedmelluq.discord.lavaplayer.track.AudioItem;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoadItemResponse {

    private AudioItem audioItem;
    public boolean failure;
    private String reason;

}