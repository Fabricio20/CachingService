package net.notfab.caching.server;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import net.notfab.caching.server.entities.security.AuthenticationFilter;
import net.notfab.caching.server.manager.AuthManager;
import net.notfab.caching.server.manager.DatabaseManager;
import net.notfab.caching.server.manager.TaskManager;
import net.notfab.caching.server.service.TrackService;
import net.notfab.caching.server.utils.Metrics;
import net.notfab.spigot.simpleconfig.SimpleConfig;
import net.notfab.spigot.simpleconfig.SimpleConfigManager;
import net.notfab.spigot.simpleconfig.standalone.StandaloneConfigManager;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class CacheServer {

    @Getter
    private static CacheServer Instance;
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Getter
    private final SimpleConfigManager configManager;
    @Getter
    private final TaskManager taskManager;

    @Getter
    private final Metrics metrics;
    @Getter
    private final DatabaseManager database;
    @Getter
    private final AuthManager authManager;

    @Getter
    private final TrackService trackService;

    private final HttpServer httpServer;

    public CacheServer() {
        Instance = this;
        // -- Core Components
        this.configManager = new StandaloneConfigManager(new File("."));
        this.taskManager = new TaskManager();
        SimpleConfig config = this.configManager.getNewConfig("config.yml");
        // -- Networking
        this.metrics = new Metrics(config, this.taskManager);
        this.database = new DatabaseManager(config);
        this.authManager = new AuthManager(this.database);
        // Services
        this.trackService = new TrackService(this.metrics, this.database.getElastic());
        // -- Tasks
        this.taskManager.scheduleAtFixedRate(() -> {
            CountRequest request = new CountRequest("youtube");
            CountResponse response;
            try {
                response = this.database.getElastic().count(request, RequestOptions.DEFAULT);
            } catch (IOException ex) {
                logger.error("Error counting index size.", ex);
                return;
            }
            this.metrics.getIndexSizeGauge().labels("youtube").set(response.getCount());
        }, 5, 60, TimeUnit.SECONDS);
        // -- Server
        URI baseURI = this.getBaseUrl(config);
        this.httpServer = startServer(baseURI);
        logger.info("Server started. " + baseURI.toASCIIString());
        // -- Console
        Scanner scanner = new Scanner(System.in);
        String cmd;
        do {
            cmd = scanner.next();
            if (cmd.equalsIgnoreCase("shutdown")) {
                cmd = null;
            }
        } while (cmd != null);
        this.shutdown();
    }

    private void shutdown() {
        this.database.shutdown();
        this.httpServer.shutdown();
    }

    private URI getBaseUrl(SimpleConfig config) {
        return UriBuilder.fromUri(config.getString("Http.URI")).port(config.getInt("Http.Port")).build();
    }

    private HttpServer startServer(URI baseURI) {
        final ResourceConfig rc = new ResourceConfig().packages("net.notfab.caching.server.rest");

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        rc.register(new JacksonJsonProvider(mapper));
        rc.register(MultiPartFeature.class);
        rc.register(AuthenticationFilter.class);

        return GrizzlyHttpServerFactory.createHttpServer(baseURI, rc);
    }

}