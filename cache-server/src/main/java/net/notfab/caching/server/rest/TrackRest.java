package net.notfab.caching.server.rest;

import net.notfab.caching.server.entities.ClientDTO;
import net.notfab.caching.server.entities.security.Secured;
import net.notfab.caching.server.service.TrackService;
import net.notfab.caching.shared.SearchParams;
import net.notfab.caching.shared.Track;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/track")
@Secured
public class TrackRest {

    @Context
    private SecurityContext securityContext;
    @HeaderParam("X-API")
    private double VERSION = 2.0;
    @HeaderParam("X-INDEX")
    private String INDEX = null;

    private static TrackService service = TrackService.getInstance();

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getTrack(@PathParam("id") String id) {
        return service.get(new ClientDTO(this.securityContext, this.INDEX, this.VERSION), id);
    }

    @POST
    @Path("/index")
    @Produces("application/json")
    public Response addToIndex(Track track) {
        return service.index(new ClientDTO(this.securityContext, this.INDEX, this.VERSION), track);
    }

    @POST
    @Path("/search")
    @Produces("application/json")
    public Response search(SearchParams params) {
        return service.search(new ClientDTO(this.securityContext, this.INDEX, this.VERSION), params);
    }

}