package net.notfab.caching.server.manager;

import net.notfab.caching.server.entities.IRedisConfig;
import net.notfab.spigot.simpleconfig.Section;
import net.notfab.spigot.simpleconfig.SimpleConfig;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final RestHighLevelClient client;
    private final JedisPool jedisPool;

    public DatabaseManager(SimpleConfig config) {
        Section database = config.getSection("Database");
        // -- ElasticSearch Cluster
        List<String> elastic = database.getStringList("Elastic");
        List<HttpHost> elasticHosts = new ArrayList<>();
        elastic.forEach(ipPort -> {
            String ip = ipPort;
            int port = 9200;
            if (ipPort.contains(":")) {
                ip = ipPort.split(":")[0];
                port = Integer.parseInt(ipPort.split(":")[1]);
            }
            elasticHosts.add(new HttpHost(ip, port, "http"));
        });
        RestClientBuilder builder = RestClient.builder(elasticHosts.toArray(new HttpHost[0]));
        this.client = new RestHighLevelClient(builder);
        // -- Redis
        Section redis = database.getSection("Redis");
        if (redis.contains("Password")) {
            this.jedisPool = new JedisPool(new IRedisConfig(), redis.getString("IP"),
                    redis.getInt("Port"), 2000, redis.getString("Password"));
        } else {
            this.jedisPool = new JedisPool(new IRedisConfig(), redis.getString("IP"), redis.getInt("Port"));
        }
    }

    public void shutdown() {
        try {
            this.client.close();
            this.jedisPool.close();
        } catch (IOException ex) {
            logger.error("Failed to close database connections", ex);
        }
    }

    public RestHighLevelClient getElastic() {
        return client;
    }

    public Jedis getRedis() {
        return this.jedisPool.getResource();
    }

}