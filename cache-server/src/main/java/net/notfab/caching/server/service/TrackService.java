package net.notfab.caching.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import net.notfab.caching.server.entities.ClientDTO;
import net.notfab.caching.server.entities.ITrackService;
import net.notfab.caching.server.utils.Metrics;
import net.notfab.caching.server.utils.RestUtils;
import net.notfab.caching.shared.SearchParams;
import net.notfab.caching.shared.Track;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Response;
import java.io.IOException;

public class TrackService implements ITrackService {

    private static final Logger logger = LoggerFactory.getLogger(TrackService.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private final RestHighLevelClient elastic;
    private final CacheControl indexCache = new CacheControl();
    private final CacheControl searchCache = new CacheControl();

    @Getter
    private final Metrics metrics;

    @Getter
    private static TrackService Instance;

    public TrackService(Metrics metrics, RestHighLevelClient elastic) {
        this.elastic = elastic;
        this.metrics = metrics;
        this.indexCache.setMaxAge(86400);
        this.searchCache.setMaxAge(3600);
        Instance = this;
    }

    @Override
    public Response get(ClientDTO client, String id) {
        GetRequest getRequest = new GetRequest(client.getIndex(), id);
        try {
            this.metrics.getIndexLoadCounter().labels(client.getName()).inc();
            GetResponse getResponse = elastic.get(getRequest, RequestOptions.DEFAULT);
            if (getResponse.isExists()) {
                this.metrics.getCacheEvents().labels("hit", client.getName()).inc();
                return Response.ok(getResponse.getSourceAsString())
                        .cacheControl(this.indexCache)
                        .build();
            } else {
                this.metrics.getCacheEvents().labels("miss", client.getName()).inc();
                return RestUtils.error(204, "Document not found.");
            }
        } catch (Exception ex) {
            logger.error("Failed to fetch document by id", ex);
            this.metrics.getCacheEvents().labels("error", client.getName()).inc();
            return RestUtils.error(500, "Failed to fetch document by ID.");
        }
    }

    @Override
    public Response index(ClientDTO client, Track track) {
        IndexRequest request = new IndexRequest(client.getIndex());
        try {
            request.id(track.getId());
            request.source(objectMapper.writeValueAsString(track), XContentType.JSON);
            request.opType(DocWriteRequest.OpType.INDEX);
            this.metrics.getIndexAddCounter().labels(client.getName()).inc();
            elastic.indexAsync(request, RequestOptions.DEFAULT, new ActionListener<>() {
                @Override
                public void onResponse(IndexResponse indexResponse) {
                }

                @Override
                public void onFailure(Exception ex) {
                    logger.error("Error while indexing " + track.getId(), ex);
                }
            });
        } catch (Exception ex) {
            logger.error("Failed to index document " + track.getId());
        }
        return Response.accepted().build();
    }

    @Override
    public Response search(ClientDTO client, SearchParams params) {
        if ((params.getAuthor().length == 0 && params.getTitle().length == 0)
                || (params.getSearch() == null || params.getSearch().isBlank())) {
            return RestUtils.error(400, "Not enough terms");
        }
        SearchRequest searchRequest = new SearchRequest(client.getIndex());
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder builder = QueryBuilders.boolQuery();

        // Title
        if (params.getTitle().length > 0) {
            for (String string : params.getTitle()) {
                builder.should(QueryBuilders.matchPhraseQuery("title", string));
            }
        }
        // Author
        if (params.getAuthor().length > 0) {
            for (String string : params.getAuthor()) {
                builder.should(QueryBuilders.matchPhraseQuery("author", string));
            }
        }

        searchSourceBuilder.query(builder);
        searchSourceBuilder.size(5);
        searchRequest.source(searchSourceBuilder);
        try {
            SearchResponse searchResponse = elastic.search(searchRequest, RequestOptions.DEFAULT);
            SearchHits hits = searchResponse.getHits();
            if (hits.getTotalHits().value < 5) {
                this.metrics.getCacheEvents().labels("miss", client.getName()).inc();
                return RestUtils.error(204, "Minimum confidence not met.");
            } else if (hits.getMaxScore() > 0.5) {
                JSONArray entity = new JSONArray();
                hits.forEach(hit -> entity.put(new JSONObject(hit.getSourceAsString())));
                this.metrics.getCacheEvents().labels("hit", client.getName()).inc();
                return Response.ok(entity.toString())
                        .cacheControl(this.searchCache)
                        .build();
            } else {
                this.metrics.getCacheEvents().labels("miss", client.getName()).inc();
                return RestUtils.error(204, "Minimum confidence not met.");
            }
        } catch (IOException ex) {
            logger.error("Error while searching", ex);
            this.metrics.getCacheEvents().labels("error", client.getName()).inc();
            return RestUtils.error(500, "Unknown error while searching");
        }
    }

}