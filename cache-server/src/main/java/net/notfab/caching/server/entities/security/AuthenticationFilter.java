package net.notfab.caching.server.entities.security;

import net.notfab.caching.server.CacheServer;
import net.notfab.caching.server.utils.RestUtils;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.security.Principal;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final String AUTHENTICATION_SCHEME = "Bearer";

    @Override
    public void filter(ContainerRequestContext requestContext) {
        // Get the Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Validate the Authorization header
        if (!isTokenBasedAuthentication(authorizationHeader)) {
            abortWithUnauthorized(requestContext, new IllegalStateException("Invalid Authentication Scheme"));
            return;
        }

        // Extract the token from the Authorization header
        String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length() + 1).trim();
        try {
            // Validate the token
            this.injectSession(this.validate(token), requestContext);
        } catch (IllegalStateException ex) {
            abortWithUnauthorized(requestContext, ex);
        }
    }

    private boolean isTokenBasedAuthentication(String authorizationHeader) {
        return authorizationHeader != null && authorizationHeader.toLowerCase()
                .startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
    }

    /*
     * Abort the filter chain with a 401 status code response
     * The WWW-Authenticate header is sent along with the response
     */
    private void abortWithUnauthorized(ContainerRequestContext requestContext, IllegalStateException exception) {
        requestContext.abortWith(RestUtils.error(401, exception.getMessage()));
    }

    /*
     * Check if the token was issued by the server and if it's not expired
     *  Throw an Exception if the token is invalid
     */
    private String validate(String token) throws IllegalStateException {
        String userName = CacheServer.getInstance().getAuthManager().getTokenFromHeader(token);
        if (userName == null) {
            throw new IllegalStateException("Invalid Token");
        }
        // Invalid
        return userName;
    }

    private void injectSession(String userName, ContainerRequestContext context) {
        final SecurityContext currentSecurityContext = context.getSecurityContext();
        context.setSecurityContext(new SecurityContext() {

            @Override
            public Principal getUserPrincipal() {
                return () -> userName;
            }

            @Override
            public boolean isUserInRole(String permission) {
                //TODO: Role control
                return true;
            }

            @Override
            public boolean isSecure() {
                return currentSecurityContext.isSecure();
            }

            @Override
            public String getAuthenticationScheme() {
                return AUTHENTICATION_SCHEME;
            }

        });
    }

}