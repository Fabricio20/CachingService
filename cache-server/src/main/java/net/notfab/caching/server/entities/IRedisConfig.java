package net.notfab.caching.server.entities;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

public class IRedisConfig extends GenericObjectPoolConfig {

    public IRedisConfig() {
        this.setTestWhileIdle(true);
        this.setMinEvictableIdleTimeMillis(60000L);
        this.setTimeBetweenEvictionRunsMillis(30000L);
        this.setNumTestsPerEvictionRun(-1);
        this.setMaxWaitMillis(-1L);
        this.setMaxTotal(1000);
    }

}