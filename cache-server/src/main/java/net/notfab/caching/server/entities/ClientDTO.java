package net.notfab.caching.server.entities;

import lombok.Data;
import net.notfab.caching.server.utils.RestUtils;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.SecurityContext;

@Data
public class ClientDTO {

    private String name;
    private double version;
    private String index;

    public ClientDTO(SecurityContext securityContext, String index, double version) {
        this.name = securityContext.getUserPrincipal().getName();
        this.version = version;
        if (index == null) {
            throw new BadRequestException(RestUtils.error(400, "Invalid Index."));
        } else if (version < 2.0) {
            throw new BadRequestException(RestUtils.error(400, "Invalid Version."));
        }
        this.index = index.toLowerCase();
    }

}