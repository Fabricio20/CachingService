package net.notfab.caching.server.utils;

import org.json.JSONObject;

import javax.ws.rs.core.Response;

public class RestUtils {

    public static Response error(int status, String message) {
        return Response.status(status).entity(new JSONObject().put("error", message).toString()).build();
    }

}