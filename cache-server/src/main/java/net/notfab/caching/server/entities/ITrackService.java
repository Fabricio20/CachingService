package net.notfab.caching.server.entities;

import net.notfab.caching.shared.SearchParams;
import net.notfab.caching.shared.Track;

import javax.ws.rs.core.Response;

public interface ITrackService {

    Response get(ClientDTO client, String id);

    Response index(ClientDTO client, Track track);

    Response search(ClientDTO client, SearchParams searchParams);

}