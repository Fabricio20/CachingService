package net.notfab.caching.server.manager;

import redis.clients.jedis.Jedis;

public class AuthManager {

    private DatabaseManager databaseManager;

    public AuthManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    public String getTokenFromHeader(String token) {
        try (Jedis jedis = this.databaseManager.getRedis()) {
            if (jedis.hexists("Tokens", token)) {
                return jedis.hget("Tokens", token);
            } else {
                return null;
            }
        }
    }

}