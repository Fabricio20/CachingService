package net.notfab.caching.server.utils;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.exporter.PushGateway;
import lombok.Getter;
import net.notfab.caching.server.manager.TaskManager;
import net.notfab.spigot.simpleconfig.SimpleConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Metrics {

    private static final Logger logger = LoggerFactory.getLogger(Metrics.class);

    @Getter
    private PushGateway pushgateway;
    @Getter
    private final CollectorRegistry registry = new CollectorRegistry();

    public Metrics(SimpleConfig config, TaskManager taskManager) {
        try {
            pushgateway = new PushGateway(new URL(config.getString("Metrics")));
        } catch (MalformedURLException ex) {
            logger.error("Malformed PushGateway URL", ex);
        }
        if (System.getenv("DEVELOPMENT") == null) {
            taskManager.scheduleAtFixedRate(() -> {
                try {
                    this.pushgateway.push(registry, "CacheService");
                    logger.debug("Pushed metrics to Prometheus.");
                } catch (Exception ex) {
                    logger.error("Error pushing data to Prometheus PushGateway", ex);
                }
            }, 15, 15, TimeUnit.SECONDS);
        } else {
            logger.info("Not posting metrics due to DEVELOPMENT mode enabled.");
        }
    }

    @Getter
    private final Counter indexAddCounter = Counter.build()
            .name("cache_tracks_indexed")
            .help("Tracks Indexed")
            .labelNames("bot")
            .register(registry);

    @Getter
    private final Counter indexLoadCounter = Counter.build()
            .name("cache_tracks_loaded")
            .help("Tracks Loaded")
            .labelNames("bot")
            .register(registry);

    @Getter
    private final Counter cacheEvents = Counter.build()
            .name("cache_events")
            .help("Cache Events")
            .labelNames("type", "bot")
            .register(registry);

    @Getter
    private final Gauge indexSizeGauge = Gauge.build()
            .name("cache_index_size")
            .help("Index Size")
            .labelNames("index")
            .register(registry);

    // ----------------- LEGACY

    @Getter
    private final Counter legacyCounter = Counter.build()
            .name("cache_legacy")
            .help("Legacy Requests")
            .labelNames("bot", "action")
            .register(registry);

}