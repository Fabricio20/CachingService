package net.notfab.caching.client;

import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.notfab.caching.shared.CacheResponse;
import net.notfab.caching.shared.SearchParams;
import net.notfab.caching.shared.YoutubeTrack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncCacheClient {

    private static final Logger logger = LoggerFactory.getLogger(AsyncCacheClient.class);

    private final ExecutorService service;
    private final CacheClient cacheClient;

    public AsyncCacheClient(String endpoint, String token) {
        this(endpoint, token, Executors.newCachedThreadPool());
    }

    public AsyncCacheClient(String endpoint, String token, ExecutorService service) {
        this.service = service;
        this.cacheClient = new CacheClient(endpoint, token, service);
    }

    /**
     * Asynchronously adds all items from a playlist to the cache.
     *
     * @param audioPlaylist LavaPlayer AudioPlayList.
     */
    public CompletableFuture<Void> addToIndex(AudioPlaylist audioPlaylist) {
        return cacheClient.addToIndex(audioPlaylist);
    }

    /**
     * Asynchronously adds a track to the cache.
     *
     * @param audioTrack LavaPlayer AudioTrack.
     */
    public CompletableFuture<Void> addToIndex(AudioTrack audioTrack) {
        return cacheClient.addToIndex(audioTrack);
    }

    /**
     * Performs search on the cache asynchronously.
     *
     * @param searchParams SearchParams with the parameters.
     *                     Title or Author is required. (Can send both for better results).
     *                     Search is always required (usually raw input string).
     * @return CompletableFuture of List of YoutubeTrack or empty list.
     */
    public CompletableFuture<List<YoutubeTrack>> search(SearchParams searchParams) {
        final CompletableFuture<List<YoutubeTrack>> future = new CompletableFuture<>();
        service.submit(() -> {
            try {
                final List<YoutubeTrack> tracks = cacheClient.search(searchParams);
                future.complete(tracks);
            } catch (final Throwable throwable) {
                future.completeExceptionally(throwable);
            }
        });
        return future;
    }

    /**
     * Retrieves metadata asynchronously for a track.
     *
     * @param id - Track id.
     * @return CompletableFuture of CacheResponse.
     */
    public CompletableFuture<CacheResponse> get(String id) {
        final CompletableFuture<CacheResponse> future = new CompletableFuture<>();
        service.submit(() -> {
            try {
                final CacheResponse cacheResponse = cacheClient.get(id);
                future.complete(cacheResponse);
            } catch (final Throwable throwable) {
                future.completeExceptionally(throwable);
            }
        });
        return future;
    }

}