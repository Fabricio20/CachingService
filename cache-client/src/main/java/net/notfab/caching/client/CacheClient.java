package net.notfab.caching.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.notfab.caching.shared.CacheResponse;
import net.notfab.caching.shared.SearchParams;
import net.notfab.caching.shared.Track;
import net.notfab.caching.shared.YoutubeTrack;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CacheClient {

    private static final Logger logger = LoggerFactory.getLogger(CacheClient.class);
    private static final String VERSION = "2.0";

    private final String ENDPOINT;
    private final String TOKEN;
    private final OkHttpClient okHttpClient;
    private final ObjectMapper objectMapper;
    private final ExecutorService service;

    public CacheClient(String endpoint, String token) {
        this(endpoint, token, Executors.newCachedThreadPool());
    }

    public CacheClient(String endpoint, String token, ExecutorService service) {
        this.ENDPOINT = endpoint;
        this.TOKEN = token;
        this.objectMapper = new ObjectMapper();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.okHttpClient = new OkHttpClient.Builder().followSslRedirects(true).build();
        this.service = service;
    }

    /**
     * Asynchronously adds all items from a playlist to the cache.
     *
     * @param audioPlaylist LavaPlayer AudioPlayList.
     */
    public CompletableFuture<Void> addToIndex(AudioPlaylist audioPlaylist) {
        final List<CompletableFuture<Void>> singleFutures = new ArrayList<>();
        audioPlaylist.getTracks().forEach(track -> singleFutures.add(addToIndex(track)));
        return CompletableFuture.allOf(singleFutures.toArray(new CompletableFuture[0]));
    }

    /**
     * Asynchronously adds a track to the cache.
     *
     * @param audioTrack LavaPlayer AudioTrack.
     */
    public CompletableFuture<Void> addToIndex(AudioTrack audioTrack) {
        final CompletableFuture<Void> future = new CompletableFuture<>();
        if (!(audioTrack instanceof YoutubeAudioTrack)) {
            future.complete(null);
            return future;
        }
        service.submit(() -> {
            Track track = YoutubeTrack.from(audioTrack);
            String trackAsString;
            try {
                trackAsString = this.objectMapper.writeValueAsString(track);
            } catch (JsonProcessingException ex) {
                logger.error("Failed to create YoutubeTrack from AudioTrack", ex);
                future.completeExceptionally(ex);
                return;
            }
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), trackAsString);
            Request request = new Request.Builder()
                    .url(ENDPOINT + "/track/index")
                    .addHeader("Authorization", "Bearer " + this.TOKEN)
                    .addHeader("X-API", VERSION)
                    .addHeader("X-INDEX", "Youtube")
                    .post(body)
                    .build();
            try (Response response = this.okHttpClient.newCall(request).execute()) {
                logger.trace("Indexed " + track.getId() + " / " + response.isSuccessful() + " / " + response.code());
            } catch (IOException ex) {
                logger.debug("Failed to send Index request to cache", ex);
                future.completeExceptionally(ex);
                return;
            }
            future.complete(null);
        });
        return future;
    }

    /**
     * Performs search on the cache.
     *
     * @param searchParams SearchParams with the parameters.
     *                     Title or Author is required. (Can send both for better results).
     *                     Search is always required (usually raw input string).
     * @return List of YoutubeTrack or empty list.
     */
    public List<YoutubeTrack> search(SearchParams searchParams) {
        String searchAsString;
        try {
            searchAsString = this.objectMapper.writeValueAsString(searchParams);
        } catch (JsonProcessingException ex) {
            logger.error("Failed to serialize search params", ex);
            return new ArrayList<>();
        }
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), searchAsString);
        Request request = new Request.Builder()
                .url(ENDPOINT + "/track/search")
                .addHeader("Authorization", "Bearer " + this.TOKEN)
                .addHeader("X-API", VERSION)
                .addHeader("X-INDEX", "Youtube")
                .post(body)
                .build();
        try (Response response = this.okHttpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                int status = response.code();
                if (status == 200) {
                    ResponseBody responseBody = response.body();
                    if (responseBody != null) {
                        return Arrays.asList(this.objectMapper
                                .readValue(responseBody.string(), YoutubeTrack[].class));
                    }
                } else if (status == 204) {
                    return new ArrayList<>();
                }
            }
        } catch (IOException ex) {
            logger.error("Failed to search on cache", ex);
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves metadata for a track.
     *
     * @param id - Track id.
     * @return CacheResponse.
     */
    public CacheResponse get(String id) {
        Request request = new Request.Builder()
                .url(ENDPOINT + "/track/" + id)
                .addHeader("Authorization", "Bearer " + this.TOKEN)
                .addHeader("X-API", VERSION)
                .addHeader("X-INDEX", "Youtube")
                .get()
                .build();
        try (Response response = this.okHttpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                int status = response.code();
                ResponseBody responseBody = response.body();
                if (responseBody != null) {
                    if (status == 200) {
                        YoutubeTrack track = this.objectMapper.readValue(responseBody.string(), YoutubeTrack.class);
                        return new CacheResponse(track, false, null);
                    } else if (status == 204) {
                        return new CacheResponse(null, true, responseBody.string());
                    }
                }
            }
        } catch (IOException ex) {
            logger.error("Failed to search on cache", ex);
            return new CacheResponse(null, true, "Failed to connect");
        }
        return new CacheResponse(null, true, "Unknown error");
    }

}