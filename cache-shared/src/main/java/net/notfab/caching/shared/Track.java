package net.notfab.caching.shared;

import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import lombok.Data;

@Data
public class Track {

    private String id;
    private String title;
    private String author;
    private Long length;
    private boolean stream = false;

    // Jackson
    public Track() {

    }

    public Track(String id) {
        this.id = id;
    }

    public String toURL() {
        throw new IllegalStateException("Not implemented.");
    }

    public AudioTrack toAudioTrack(AudioSourceManager audioSourceManager) {
        throw new IllegalStateException("Not Implemented.");
    }

}