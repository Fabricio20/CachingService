package net.notfab.caching.shared;

import com.sedmelluq.discord.lavaplayer.source.AudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import com.sedmelluq.discord.lavaplayer.track.info.AudioTrackInfoBuilder;

public class YoutubeTrack extends Track {

    // Jackson
    public YoutubeTrack() {

    }

    public YoutubeTrack(String id) {
        super(id);
    }

    @Override
    public String toURL() {
        return "https://www.youtube.com/watch?v=" + this.getId();
    }

    @Override
    public AudioTrack toAudioTrack(AudioSourceManager audioSourceManager) {
        AudioTrackInfo info = AudioTrackInfoBuilder.empty()
                .setTitle(this.getTitle())
                .setAuthor(this.getAuthor())
                .setLength(this.getLength())
                .setIdentifier(this.getId())
                .setIsStream(this.isStream())
                .setUri(this.toURL())
                .build();
        return new YoutubeAudioTrack(info, (YoutubeAudioSourceManager) audioSourceManager);
    }

    public static YoutubeTrack from(AudioTrack audioTrack) {
        YoutubeTrack track = new YoutubeTrack(audioTrack.getInfo().identifier);
        track.setTitle(audioTrack.getInfo().title);
        track.setAuthor(audioTrack.getInfo().author);
        track.setLength(audioTrack.getInfo().length);
        track.setStream(audioTrack.getInfo().isStream);
        return track;
    }

}