package net.notfab.caching.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CacheResponse {

    private Track track;
    public boolean failure;
    private String reason;

}
