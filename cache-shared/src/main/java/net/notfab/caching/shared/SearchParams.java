package net.notfab.caching.shared;

import lombok.Getter;

/**
 * Represents a search.
 * <p>
 * Title or Author is required. (Can send both for better results).
 * Search is always required (usually raw input string).
 */
@Getter
public class SearchParams {

    private String[] title = new String[0];
    private String[] author = new String[0];
    private String search;

    public SearchParams setTitle(String... title) {
        this.title = title;
        return this;
    }

    public SearchParams setAuthor(String... author) {
        this.author = author;
        return this;
    }

    public SearchParams setSearch(String search) {
        this.search = search;
        return this;
    }

}